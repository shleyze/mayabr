import React from "react";
import Widget from './widget';

export default class List extends React.Component {
    render() {

        return (
            <div className="widget-list">
                {
                    this.props.widgets.map((item, index) => {
                        return <Widget
                            key={index}
                            name={item.name}
                            type={item.type}
                            degree={item.degree}
                            wind={item.wind}
                            pressure={item.pressure}
                            onDelete={this.props.onDeleteWidget}
                        />
                    })
                }
            </div>
        )
    }
}