import React from "react";

export default class Widget extends React.Component {
    render() {
        let icon_type = '';

        switch(this.props.type) {
            case 'sunny':
                icon_type = 'widget__icon_type_sunny';
                break;
            case 'rainy':
                icon_type = 'widget__icon_type_rainy';
                break;
        }

        return (
            <div className="widget">
                <div className="widget__title">
                    {this.props.name}
                </div>

                <div className="widget__group">
                    <div className={`widget__icon ${icon_type}`} />

                    <div className="widget__property widget__property_size_large">
                        {this.props.degree}
                        °C
                    </div>
                </div>

                <div className="widget__property">
                    Ветер: {this.props.wind} м/с
                </div>

                <div className="widget__property">
                    Давление: {this.props.pressure} мм
                </div>

                <div className="widget__delete" onClick={this.props.onDelete.bind(this, this.props)} />
            </div>
        )
    }
}