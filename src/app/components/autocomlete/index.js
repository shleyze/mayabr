import React from "react";
import { Typeahead }  from 'react-typeahead';
import './styles.css';

export class Autocomplete extends React.Component {
    render() {
        return (
            <Typeahead
                customClasses={{
                    input: 'typeahead__input',
                    results: 'typeahead__result',
                    listItem: 'typeahead__result-item',
                    listAnchor: 'fsdfsd',
                    hover: 'fsd',
                    typeahead: 'fsd',
                    resultsTruncated: 'fsd'
                }}
                ref={(origin) => { this.origin = origin; }}
                {...this.props}
            />
        )
    }
}