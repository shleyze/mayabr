import React from 'react';
import { connect } from 'react-redux'
import { Autocomplete } from '../../components/autocomlete'
import { Button } from '../../components/button'
import { List as WeatherList } from '../../components/weather';
import DATA from '../../state/DATA.json';
import './styles.css';


export class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedWidget: {},
            widgets: []
        };
    }

    onOptionSelected(option) {
        this.setState({
            selectedWidget: option
        });
    }
    isWeatherWidgetInList(widget) {
        let isAlreadyAdded = false;

        this.state.widgets.some((item) => {
            if(widget.name === item.name) isAlreadyAdded = true;
        });
        return isAlreadyAdded;
    }

    addWeatherWidget() {
        let widgets = this.state.widgets;
        let selectedWidget = this.state.selectedWidget;

        if (Object.keys(selectedWidget).length === 0) return;

        if (this.isWeatherWidgetInList(selectedWidget) === true) return;

        widgets.push(selectedWidget);


        this.setState({
            widgets: widgets
        });

        this.input.origin.setState({
            entryValue: '',
        });

        this.props.onAddWidget(selectedWidget);
    }

    deleteWeatherWidget(widget) {
        let widgets = this.state.widgets;

        let actualWidgets = widgets.filter(function(item) {
            return widget.name !== item.name;
        });


        this.setState({
            widgets: actualWidgets
        });

        this.props.onDeleteWidget(widget);
    }

    render() {
        return (
            <div>
                <Autocomplete
                    suggestions={DATA}
                    options={DATA}
                    placeholder='Город'
                    filterOption='name'
                    displayOption={(option) => option.name}
                    ref={(input) => { this.input = input; }}
                    onOptionSelected={this.onOptionSelected.bind(this)}
                />

                <Button
                    onClick={this.addWeatherWidget.bind(this)}>
                    <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAAAAABWESUoAAAAaklEQVQ4y2P4TwAwUFvBtrbeW3gVmDMwzMGrwJeBYTleBf4jQ0E0A8M+TAV/lk+fDwWGDAwZi6HsSSdhCr4zYAfRRCv4M793CgRM12NgSJwO5bQdwu7IQ6ORRYQCa0LJvsbceh+N8yYmAAATqnaCJkjchQAAAABJRU5ErkJggg=='/>
                </Button>

                {
                    this.state.widgets.length ? <input type="range" min="0" max="100" step="5" defaultValue="50" /> : null
                }

                {
                    this.state.widgets.length ? <WeatherList widgets={this.state.widgets} onDeleteWidget={this.deleteWeatherWidget.bind(this)} /> : null
                }

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        widgets: state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onAddWidget: (widget) => {
            dispatch({
                type: 'ADD_WIDGET',
                payload: widget
            })
        },
        onDeleteWidget: (widget) => {
            dispatch({
                type: 'DELETE_WIDGET',
                payload: widget
            })
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)