import { createStore } from 'redux';

const initialState = {};

const app = (state = initialState, action) => {
    if (action.type === 'ADD_WIDGET') {
        return [
            ...state,
            action.payload
        ];
    }
    if (action.type === 'DELETE_WIDGET') {
        return [
            ...state,
            action.payload
        ];
    }
    return state;
};


const store = createStore(
    app,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;