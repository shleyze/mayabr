import React from "react";
import { render } from 'react-dom';
import { Provider as ReduxProvider } from "react-redux";
import store from '../app/state/store'

import App from "../app/views/pages";

render(
    <ReduxProvider store={store}>
        <App/>
    </ReduxProvider>,
    document.getElementById('root')
);