const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin")

const DIR = {
    SRC: path.resolve('./src'),
    APP: path.resolve('./src/app'),
    DEV: path.resolve('./dev')
};

module.exports = {
    entry: {
        main: [
            `${DIR.SRC}/client`
        ],
        vendor: ['react', 'react-dom']
    },
    output: {
        path: `${DIR.DEV}/js`,
        filename: '[name].js',
        publicPath: 'js'
    },
    devtool: '#inline-source-map',
    watch: true,
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            },
            {
                test: /\.js$/,
                include: DIR.SRC,
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: '../index.html',
            template: `${DIR.APP}/views/pages/index.html`,
            hash: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor"
        }),
        new ExtractTextPlugin("../css/styles.css")
    ]
};